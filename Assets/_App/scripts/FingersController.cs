//
// Fingers Gestures
// (c) 2015 Digital Ruby, LLC
// Source code may be used for personal or commercial projects.
// Source code may NOT be redistributed or sold.
// 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using static UnityEngine.SceneManagement.SceneManager;

namespace DigitalRubyShared
{
    public struct BaseTransform
    {
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;

        public BaseTransform(Vector3 position, Vector3 rotation, Vector3 scale)
        {
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
        }
    }

    public class FingersController : MonoBehaviour
    {
        private const int SceneBuildIndex = 2;
        public float rotationFactor = 1.5f;
        public float scaleFactor = 5;
        public float moveFactor = 0.01f;
        [FormerlySerializedAs("ModelCenterTR")] public Transform ModelMovableCenterTR;
        public Transform MiaTR;
        public RelativeToVufCamera RelativeToVufCamera;

        private BaseTransform miaBaseTransform;

        // public UnityEngine.UI.Text dpiLabel;
        public UnityEngine.UI.Text bottomLabel;

        private TapGestureRecognizer doubleTapGesture;
        private PanGestureRecognizer panGesture;
        private ScaleGestureRecognizer scaleGesture;
        private RotateGestureRecognizer rotateGesture;


        private readonly List<Vector3> swipeLines = new List<Vector3>();


        private void DebugText(string text, params object[] format)
        {
            bottomLabel.text = string.Format(text, format);
#if UNITY_EDITOR
            Debug.Log(string.Format(text, format));
#endif
        }


        private void PanGestureCallback(GestureRecognizer gesture)
        {
            if (gesture.State == GestureRecognizerState.Executing)
            {
                DebugText("moved, Location: {0}, {1}, Delta: {2}, {3}", gesture.FocusX, gesture.FocusY, gesture.DeltaX,
                    gesture.DeltaY);

                float deltaX = panGesture.DeltaX / 25.0f;
                float deltaY = panGesture.DeltaY / 25.0f;
                Vector3 pos = ModelMovableCenterTR.position;
                pos.x += deltaX * moveFactor;
                pos.y += deltaY * moveFactor;
                ModelMovableCenterTR.position = pos;
            }
        }

        private void ScaleGestureCallback(GestureRecognizer gesture)
        {
            if (gesture.State == GestureRecognizerState.Executing)
            {
                DebugText("Scaled: {0}, Focus: {1}, {2}", scaleGesture.ScaleMultiplier, scaleGesture.FocusX,
                    scaleGesture.FocusY);
                
                float scale;
                if (scaleGesture.ScaleMultiplier >= 1)
                {
                    scale = (scaleGesture.ScaleMultiplier-1 ) * scaleFactor;
                    MiaTR.localScale += new Vector3(scale, scale, scale);
                }
                else
                {
                    scale = (1 - scaleGesture.ScaleMultiplier) * scaleFactor;
                    MiaTR.localScale -= new Vector3(scale, scale, scale);
                }
            }
        }


        private void RotateGestureCallback(GestureRecognizer gesture)
        {
            if (gesture.State == GestureRecognizerState.Executing)
            {
                DebugText("rotate: {0}, ", rotateGesture.RotationRadiansDelta);
                MiaTR.Rotate(0.0f, rotateGesture.RotationRadiansDelta * Mathf.Rad2Deg * rotationFactor, 0.0f);
            }
        }

        private void DoubleTapGestureCallback(GestureRecognizer gesture)
        {
            if (gesture.State == GestureRecognizerState.Ended)
            {
                DebugText("Double tapped at {0}, {1}", gesture.FocusX, gesture.FocusY);
                LoadScene(GetActiveScene().buildIndex, UnityEngine.SceneManagement.LoadSceneMode.Single);

                /*ResetMia();
                RelativeToVufCamera.ResetMia();*/
            }
        }

        public void ResetMia()
        {
            ModelMovableCenterTR.position = miaBaseTransform.position;
            MiaTR.rotation = Quaternion.Euler(miaBaseTransform.rotation);
            MiaTR.localScale = miaBaseTransform.scale;
        }

        private void CreateDoubleTapGesture()
        {
            doubleTapGesture = new TapGestureRecognizer();
            doubleTapGesture.NumberOfTapsRequired = 2;
            doubleTapGesture.StateUpdated += DoubleTapGestureCallback;
            // doubleTapGesture.RequireGestureRecognizerToFail = tripleTapGesture;
            FingersScript.Instance.AddGesture(doubleTapGesture);
        }

        private void CreatePanGesture()
        {
            panGesture = new PanGestureRecognizer();
            panGesture.MinimumNumberOfTouchesToTrack = 1;
            panGesture.StateUpdated += PanGestureCallback;
            FingersScript.Instance.AddGesture(panGesture);
        }


        private void CreateScaleGesture()
        {
            scaleGesture = new ScaleGestureRecognizer();
            scaleGesture.StateUpdated += ScaleGestureCallback;
            FingersScript.Instance.AddGesture(scaleGesture);
        }

        private void CreateRotateGesture()
        {
            rotateGesture = new RotateGestureRecognizer();
            rotateGesture.StateUpdated += RotateGestureCallback;
            FingersScript.Instance.AddGesture(rotateGesture);
        }

        private static bool? CaptureGestureHandler(GameObject obj)
        {
            // I've named objects PassThrough* if the gesture should pass through and NoPass* if the gesture should be gobbled up, everything else gets default behavior
            if (obj.name.StartsWith("PassThrough"))
            {
                // allow the pass through for any element named "PassThrough*"
                return false;
            }
            else if (obj.name.StartsWith("NoPass"))
            {
                // prevent the gesture from passing through, this is done on some of the buttons and the bottom text so that only
                // the triple tap gesture can tap on it
                return true;
            }

            // fall-back to default behavior for anything else
            return null;
        }

        private void Start()
        {
            miaBaseTransform = new BaseTransform(Vector3.zero, new Vector3(0, 90, 0), Vector3.one);
            CreateDoubleTapGesture();
            CreatePanGesture();
            CreateScaleGesture();
            CreateRotateGesture();
            //ResetMia();
        }

        private void LateUpdate()
        {
            string touchIds = string.Empty;
            int gestureTouchCount = 0;
            foreach (GestureRecognizer g in FingersScript.Instance.Gestures)
            {
                gestureTouchCount += g.CurrentTrackedTouches.Count;
            }

            foreach (GestureTouch t in FingersScript.Instance.CurrentTouches)
            {
                touchIds += ":" + t.Id + ":";
            }
        }
    }
}