using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSwitcher : MonoBehaviour
{
    // Start is called before the first frame update

    public int scene2DIndex;
    public int scene3DIndex;
    public int scene3_5DIndex;
    

    public void Open2DScene()
    {
        SceneManager.LoadScene(scene2DIndex);
    }

    public void Open3DScene()
    {
        SceneManager.LoadScene(scene3DIndex);
    }

    public void Open3D_5Scene()
    {
        SceneManager.LoadScene(scene3_5DIndex);
    }


}