using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorAnimator : MonoBehaviour
{
    public string animName = "Attack01";
    public int animLayer = 0;

    public Animator _animator;

    void OnValidate()
    {
        try
        {
            initPlayer();
            setAnimationFrame(animName, animLayer);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void initPlayer()
    {
        if (_animator == null)
        {
            _animator = this.GetComponent<Animator>();
        }
    }


    private void setAnimationFrame(string animationName, int layer)
    {
        if (_animator != null)
        {
            _animator.speed = 0f;
            _animator.Play(animationName, layer);
            Debug.Log("init");
        }
    }
    
    public void Update()
    {
       // Debug.Log("sss");
        _animator.Update(Time.deltaTime);
    }
}