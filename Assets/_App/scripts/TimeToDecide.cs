using System.Collections;
using System.Collections.Generic;
//using UnityEditor.VersionControl;
using UnityEngine;

public class TimeToDecide : StateMachineBehaviour
{
    //public int loop = 0;

    public int states;
    private static readonly int ToDecide = Animator.StringToHash("timeToDecide");
    private static readonly int Brain = Animator.StringToHash("brain");
    private static readonly int Loops = Animator.StringToHash("loops");
    private static readonly int isWalking = Animator.StringToHash("isWalking");
    public int loopLimit;


    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       int loop = animator.GetInteger(Loops);
        loop++;
      //  Debug.Log("exit"+loop);
    
        if (loop > loopLimit)
        {
            loop = 0;
            var stateIndex = Decide();
            animator.SetInteger(Brain, stateIndex);
            animator.SetBool(ToDecide, true);
            animator.SetBool(isWalking, false);
        }
        animator.SetInteger(Loops, loop);
        
    }

    int Decide()
    {
        return Random.Range(0, states)+1;
    }



    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
