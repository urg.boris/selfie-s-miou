using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class App3D_5 : ScreenShooter
{
    // Start is called before the first frame update
    




    private new void Start()
    {
        base.Start();
        takePicButton.onClick.AddListener(CatifyPicture);
    }
    
    
    private void OnDestroy()
    {
        takePicButton.onClick.RemoveAllListeners();

    }
    
    public void CatifyPicture()
    {
        if (catifyingInProgress)
        {
            return;
        }

        Debug.Log("catify");
        catifyingInProgress = true;
        
        TakePic(CatifyEnd);
    }

    public void CatifyEnd()
    {
        Debug.Log("catifyend");
        catifyingInProgress = false;
    }

}
