using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class App3D : ScreenShooter
{
    // Start is called before the first frame update


    public  Transform editableTransform;
    public Slider sliderX;
    public Vector2 XLimits;
    public Slider sliderY;
    public Vector2 YLimits;
    public Slider sliderZ;
    public Vector2 ZLimits;
    public Vector3 origPos;


    private new void Start()
    {
        base.Start();
        sliderX.onValueChanged.AddListener(OnSliderXChanged);
        sliderY.onValueChanged.AddListener(OnSliderYChanged);
        sliderZ.onValueChanged.AddListener(OnSliderZChanged);
        catifyingInProgress = false;
        takePicButton.onClick.AddListener(CatifyPicture);
       // origPos = transform.position;
       sliderX.value = 0.5f;
       sliderZ.value = 0.5f;
       sliderY.value = 0.5f;
    }
    
    
    private void OnDestroy()
    {
        takePicButton.onClick.RemoveAllListeners();
        sliderX.onValueChanged.RemoveAllListeners();
        sliderY.onValueChanged.RemoveAllListeners();
        sliderZ.onValueChanged.RemoveAllListeners();
    }
    
    public void OnSliderXChanged(float value)
    {
        var newPos = editableTransform.localPosition;
        newPos.x = Mathf.Lerp(XLimits.x, XLimits.y, value);
        editableTransform.localPosition = newPos;
    }

    public void OnSliderYChanged(float value)
    {
        var newPos = editableTransform.localPosition;
        newPos.y = Mathf.Lerp(YLimits.x, YLimits.y, value);
        editableTransform.localPosition = newPos;
    }
    
    public void OnSliderZChanged(float value)
    {
        var newPos = editableTransform.localPosition;
        newPos.z = Mathf.Lerp(ZLimits.x, ZLimits.y, value);
        editableTransform.localPosition = newPos;
    }
    
    
    public void CatifyPicture()
    {
        if (catifyingInProgress)
        {
            return;
        }

        Debug.Log("catify");
        catifyingInProgress = true;
        
        TakePic(CatifyEnd);
    }

    public void CatifyEnd()
    {
        Debug.Log("catifyend");
        catifyingInProgress = false;
    }

}
