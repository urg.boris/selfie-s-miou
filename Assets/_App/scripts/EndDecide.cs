using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDecide : StateMachineBehaviour
{
    private static readonly int ToDecide = Animator.StringToHash("timeToDecide");
    private static readonly int Brain = Animator.StringToHash("brain");
    private static readonly int Loops = Animator.StringToHash("loops");
    private static readonly int isWalking = Animator.StringToHash("isWalking");

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(ToDecide, false);
        animator.SetInteger(Loops, 0);
//        Debug.Log("start");
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(isWalking, true);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
