using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class App3Dgestures : ScreenShooter
{
    // Start is called before the first frame update


    private new void Start()
    {
        base.Start();
        catifyingInProgress = false;
        takePicButton.onClick.AddListener(CatifyPicture);
    }
    
    
    private void OnDestroy()
    {
        takePicButton.onClick.RemoveAllListeners();
    }
  
    
    public void CatifyPicture()
    {
        if (catifyingInProgress)
        {
            return;
        }

        meow.DoMeow();
        catifyingInProgress = true;
        
        TakePic(CatifyEnd);
    }

    public void CatifyEnd()
    {
       // Debug.Log("catifyend");
        catifyingInProgress = false;
    }

}
