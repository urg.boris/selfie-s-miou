using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meow : MonoBehaviour
{
    public AudioClip[] meows;

    public AudioSource audioSource;
    // Start is called before the first frame update
    public void DoMeow()
    {
        
        var index = Random.Range(0, meows.Length);
//        Debug.Log(index);
        audioSource.PlayOneShot(meows[index]);
    }
}
