using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenShooter : MonoBehaviour
{
    public const string URL = "http://selfie.smislsoft.online/";
    
    public Text debug;
    private Camera _camera;
    public Button backButton;

    private const string albumName = "SelfieSMiou";
    public Canvas canvasToHide;
    public RawImage takenImage;
    public GameObject takenImageGO;
    private readonly WaitForSeconds _takeScreenshotAndSave = new WaitForSeconds(2);
    protected bool catifyingInProgress;
    public Button takePicButton;
    public Meow meow;

    protected void Start()
    {
        _camera = Camera.main;
        backButton.onClick.AddListener(GoBack);
        //Debug.Log("start2");
    }

    private void OnDestroy()
    {
        backButton.onClick.RemoveAllListeners();
    }

    public void GoBack()
    {
        SceneManager.LoadScene (0);
    }

    public void TakePic(Action callback = null)
    {
        // Take a screenshot and save it to Gallery/Photos
       // DebugLog("taking screen shot");
        StartCoroutine(TakeScreenshotAndSave(callback));
        /*  }
          else
          {
              // Don't attempt to pick media from Gallery/Photos if
              // another media pick operation is already in progress
              if (NativeGallery.IsMediaPickerBusy())
                  return;

              if (Input.mousePosition.x < Screen.width * 2 / 3)
              {
                  DebugLog("show catImage");
                  // Pick a PNG catImage from Gallery/Photos
                  // If the selected catImage's width and/or height is greater than 512px, down-scale the catImage
                  PickImage(512);
              }
              else
              {
                  // Pick a video from Gallery/Photos
                  PickVideo();
              }
          }*/
        // }
    }

    protected void DebugLog(string msg)
    {
        debug.text = msg;
    }

    protected IEnumerator TakeScreenshotAndSave(Action callback = null)
    {
        canvasToHide.gameObject.SetActive(false);
        yield return new WaitForEndOfFrame();


        Texture2D capturedTexture;
        // Save the screenshot to Gallery/Photos
        DateTime theTime = DateTime.Now;
        string datetime = theTime.ToString("yyyy-MM-dd-HH-mm-ss");
        var fileName = "mia-" + datetime + ".jpg";
        byte[] bytes;
#if UNITY_EDITOR
        capturedTexture = ScreenCapture.CaptureScreenshotAsTexture();
        bytes = capturedTexture.EncodeToJPG();

        System.IO.File.WriteAllBytes(Application.persistentDataPath+"/"+fileName, bytes);
#else
        capturedTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        capturedTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        capturedTexture.Apply();
        NativeGallery.Permission permission = NativeGallery.SaveImageToGallery(capturedTexture, albumName, fileName,
            (success, path) => Debug.Log("Media save result: " + success + " " + path));
        bytes = capturedTexture.EncodeToJPG();
        DebugLog("Permission result: " + permission);

#endif
        takenImage.texture = capturedTexture;
        takenImageGO.SetActive(true);
        //StartCoroutine(UploadAFile(bytes));
        yield return _takeScreenshotAndSave;
        takenImageGO.SetActive(false);
        // To avoid memory leaks
        Destroy(capturedTexture);
        canvasToHide.gameObject.SetActive(true);
        callback?.Invoke();
    }
    
//Our coroutine takes a screenshot of the game
   /* public IEnumerator UploadAFile(byte[] bytes)
    {
        yield return new WaitForEndOfFrame();

        // Create a Web Form, this will be our POST method's data
        var form = new WWWForm();
        form.AddBinaryData("file", bytes, "screenshot.jpg", "image/jpg");

        //POST the screenshot to GameSparks
        UnityWebRequest www = UnityWebRequest.Post(URL, form);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
//            Debug.Log("Form upload complete! " + www.downloadHandler.text);

        }
    }*/
}