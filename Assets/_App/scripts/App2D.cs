using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class App2D : ScreenShooter
{
    // Start is called before the first frame update
    public Sprite[] sprites;
    public Image catImage;
    public RectTransform catImageRC;
    public RectTransform catImageParent;
    /*public Button takePicButton;
    public Meow meow;
    private bool catifyingInProgress;*/
    private new void Start()
    {
        base.Start();
        catImage.gameObject.SetActive(false);
        catifyingInProgress = false;
        takePicButton.onClick.AddListener(CatifyPicture);
       // Debug.Log("start1");
    }
    
    private void OnDestroy()
    {
        takePicButton.onClick.RemoveAllListeners();
    }

    public void CatifyPicture()
    {
//        Debug.Log("catifystart");
        if (catifyingInProgress)
        {
            return;
        }
        meow.DoMeow();
        //Debug.Log("catify");
        catifyingInProgress = true;
        catImage.gameObject.SetActive(true);
        var sizeDelta = catImageParent.sizeDelta;
        var parentWidth = sizeDelta.x;
        var parentHeight = sizeDelta.y;
        var width = Random.Range(0.4f * parentWidth, 0.72f * parentWidth);
        var sprite = sprites[Random.Range(0, sprites.Length)];
        catImageRC.sizeDelta = new Vector2(width, width);
        catImage.sprite = sprite;
        catImage.preserveAspect = true;
        catImageRC.anchoredPosition = new Vector2(Random.Range(-parentWidth/2, parentWidth/2), -Random.Range(-parentHeight/2, parentHeight/2));
        catImageRC.rotation = Quaternion.Euler(0, 0, Random.Range(-45, 45));
        TakePic(CatifyEnd);
    }

    public void CatifyEnd()
    {
        //Debug.Log("catifyend");
        catifyingInProgress = false;
        catImage.gameObject.SetActive(false);
    }
}
