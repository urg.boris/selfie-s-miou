using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimation : MonoBehaviour
{
    public Animation animationComponent;
    private float passedTimeFromLastAnim = 0;
    private float[] changeAnimAfterRange = new[] {5f, 10f};
    private float changeAnimAfter;
    private string[] animationNames = new[] {"Idle", "Walk", "Run", "Jump", "Itching", "meow", "IdleSit"};
    // Start is called before the first frame update
    void Start()
    {
        PlayRandom();
    }

    void PlayRandom()
    {
        var index = Random.Range(0, animationNames.Length - 1);
        animationComponent.Play(animationNames[index]);
        changeAnimAfter = Random.Range(changeAnimAfterRange[0], changeAnimAfterRange[1]);
        passedTimeFromLastAnim = 0;

    }

    // Update is called once per frame
    void Update()
    {
        passedTimeFromLastAnim += Time.deltaTime;
        if (passedTimeFromLastAnim > changeAnimAfter)
        {
            PlayRandom();
        }
    }
}
