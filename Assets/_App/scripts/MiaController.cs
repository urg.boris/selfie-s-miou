using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Animations;
using UnityEngine;

public class MiaController : MonoBehaviour
{
   // public int walkingStateIndex;
    public Animator animator;
    public float radius;
    public float angle;
    public Vector3 origPos;
    private float origRot;
    public float speed;
    private static readonly int ToDecide = Animator.StringToHash("timeToDecide");
    private static readonly int isWalkingHash = Animator.StringToHash("isWalking");

/*
bool isWalking = 
{
    get true;//animator.GetBool(ToDecide) ==IStructuralEquatable;
}*/
    // Start is called before the first frame update
    void Start()
    {
        origRot = transform.localRotation.eulerAngles.y;
        origPos = transform.localPosition;
        //angle = Random.Range(0, 2*Mathf.PI);
        UpdateAcordingToAngle();
    }

    void UpdateAcordingToAngle()
    {
        var x = Mathf.Sin(angle) * radius;
        var z = Mathf.Cos(angle) * radius;
        var newPos = origPos+new Vector3(x,0,z);
        transform.localPosition = newPos;
        var rotation = origRot+ angle *180/Mathf.PI ;
        transform.localRotation = Quaternion.Euler(0,rotation,0);
    }

    // Update is called once per frame
    void Update()
    {
        bool isWalking = animator.GetBool(isWalkingHash) == true;

        if (isWalking)
        {
            angle += speed * Time.deltaTime;
            UpdateAcordingToAngle();
//            Debug.Log("isWalking" + isWalking);
         /*   var position = transform.position;
            position = new Vector3(position.x + 0.1f * Time.deltaTime, position.y, position.z);
            transform.position = position;*/
        }
    }
}