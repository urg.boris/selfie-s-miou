using System;
using System.Collections;
using System.Collections.Generic;
using DigitalRubyShared;
using UnityEngine;
using UnityEngine.Serialization;

public class RelativeToVufCamera : MonoBehaviour
{
    public Camera vufCamera;
    public FingersController FingersController;
    [FormerlySerializedAs("radius")] public float distance = 5;

    private bool iniMiaReset = false;
    // Update is called once per frame
    private void Start()
    {
        iniMiaReset = false;
    }

    public void ResetMia()
    {
        iniMiaReset = false;
    }

    void Update()
    {

        var transform1 = vufCamera.transform;
        var position = transform1.position + transform1.rotation * new Vector3(0.0f, 0.0f, distance);
        transform.position = position;
        if (!iniMiaReset)
        {
            FingersController.ResetMia();
            iniMiaReset = true;
        }
    }
}
