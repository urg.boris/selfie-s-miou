<?php

declare(strict_types=1);

namespace App\Module\Admin\Presenters;

use Nette;
use Nette\Application\UI\Form;


final class SendPresenter extends Nette\Application\UI\Presenter
{
    public function createComponentSendForm(): Form
    {
        $form = new Form();
        //$link = $this->link("//:Api:Homepage:default");
        $link = "http://selfie.smislsoft.online/";
        $form->setAction($link);
        $form->addUpload('image', 'image')
            ->addRule(Form::IMAGE, 'Image must be JPEG, PNG or GIF.');
        $form->addSubmit('submit', 'submit');
        return $form;
    }

    public function actionDefault()
    {

    }
}
