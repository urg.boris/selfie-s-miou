<?php

declare(strict_types=1);

namespace App\Module\Admin\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Finder;


final class ListPresenter extends Nette\Application\UI\Presenter
{
    private $wwwDir;

    public function __construct($wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }

    public function actionDefault()
    {
        $this->getHttpResponse()->setHeader('Pragma', 'no-cache');

        foreach (Finder::findFiles('*.jpg')->in($this->wwwDir.'/images/') as $key => $file) {

            $this->template->files[] = $file->getFilename();
        }
    }
}
