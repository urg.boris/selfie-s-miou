<?php

declare(strict_types=1);

namespace App\Module\Api\Presenters;

use Nette;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private $wwwDir;

    public function __construct($wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }

    public function actionDefault()
    {
        $images = $this->getHttpRequest()->getFiles();

        if($images) {
            foreach ($images as $image) {

                if ($image->isOk() && $image->isImage()){
                    $path = $this->wwwDir.'/images/'.Nette\Utils\Random::generate(20).'.jpg';
                    $image->move($path);
                }
            }
            $this->sendJson(["result"=>"ok"]);
        } else {
            $this->sendJson(["result"=>"nodata"]);
        }
    }
}
